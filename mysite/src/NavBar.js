import React from 'react';
import logo from './logo.svg';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Jumbotron,
  Container, Card, Button, CardImg, CardTitle, CardText, CardDeck,
 CardSubtitle, CardBody
  } from 'reactstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import About from './pages/About'
import Home from './App';
import Projects from './pages/projects'


export default class NavBar extends React.Component {
  render(){
    return (
      <div>
        <Router>
          <Navbar color="dark" dark>
            <NavbarBrand href="/">Home </NavbarBrand>
              <Nav pills>
                <NavItem>
                  <NavLink href="/Projects">Projects</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/About">About</NavLink>
                </NavItem>
              </Nav>
          </Navbar>
        <Route exact path="/" component={Home} />
        <Route path="/About" component={About} />
        <Route path="/Projects" component={Projects} />
        </Router>
      </div>
    );
  }
}
