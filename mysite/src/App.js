import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Jumbotron, Container, Card, Button, CardImg, CardTitle, CardText,
        CardHeader, CardDeck, CardColumns, CardSubtitle, CardBody,
        Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { BrowserRouter as Router, Route, Link , Switch,BrowserHistory } from 'react-router-dom';

export default class Home extends React.Component {
  render (){
    return (
      <div>
        <div className="bg">
          <Container style={{color: 'white', paddingTop: '3rem'}}>
            <h3>Welcome</h3>
            <br/>
            <p>The air we breathe is becoming increasingly polluted.
            <br/>Click around to learn more.
            <br/></p>
          </Container>
        </div>
      </div>
    );
  }
}
