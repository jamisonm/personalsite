import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'

export default class About extends Component {
    render(){
        return(
            <div>
                <h1 className="About">About</h1>
                My name is Jamison Miles and I am a computer scientist currently attending the University of Texas at Austin.
            </div>
        );
    }
}
